package br.com.mauda.seminario.cientificos.model;

//Autor: Joao Rocha

import java.time.LocalDateTime;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class Inscricao {

    private Long id;
    private Boolean direitoMaterial;
    private LocalDateTime dataCriacao;
    private LocalDateTime dataCompra;
    private LocalDateTime dataCheckIn;
    private SituacaoInscricaoEnum situacao;
    private Seminario seminario;
    private Estudante estudante;

    public Inscricao(Seminario seminario) {
        this.seminario = seminario;
        this.seminario.adicionarInscricao(this);
        this.situacao = SituacaoInscricaoEnum.DISPONIVEL;
        this.dataCriacao = LocalDateTime.now();
    }

    public void comprar(Estudante estudante, Boolean direitoMaterial) {
        this.direitoMaterial = direitoMaterial;
        this.estudante = estudante;
        this.situacao = SituacaoInscricaoEnum.COMPRADO;
        this.estudante.adicionarInscricao(this);
        this.dataCompra = LocalDateTime.now();
    }

    public void cancelarCompra() {
        this.estudante.removeInscricao(this);
        this.estudante = null;
        this.direitoMaterial = null;
        this.situacao = SituacaoInscricaoEnum.DISPONIVEL;
        this.dataCompra = null;
    }

    public void realizarCheckIn() {
        this.situacao = SituacaoInscricaoEnum.CHECKIN;
        this.dataCheckIn = LocalDateTime.now();
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public void setDireitoMaterial(Boolean direitoMaterial) {
        this.direitoMaterial = direitoMaterial;
    }

    public LocalDateTime getDataCriacao() {
        return this.dataCriacao;
    }

    public LocalDateTime getDataCompra() {
        return this.dataCompra;
    }

    public SituacaoInscricaoEnum getSituacaoInscricao() {
        return this.situacao;
    }

    public void setSituacaoInscricao(SituacaoInscricaoEnum situacaoInscricao) {
        this.situacao = situacaoInscricao;
    }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacao;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    public LocalDateTime getDataCheckIn() {
        return this.dataCheckIn;
    }

    public Seminario getSeminario() {
        return this.seminario;
    }

    public void setSeminario(Seminario seminario) {
        this.seminario = seminario;
    }
}
