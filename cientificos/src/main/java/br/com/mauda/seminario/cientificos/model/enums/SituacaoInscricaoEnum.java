package br.com.mauda.seminario.cientificos.model.enums;

//Autor: Joao Rocha

public enum SituacaoInscricaoEnum {

    DISPONIVEL(null, "Disponivel"),
    COMPRADO(null, "Comprado"),
    CHECKIN(null, "Checkin");

    private Long id;
    private String nome;

    private SituacaoInscricaoEnum(Long id, String nome) {
        this.id = id;

    }

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

}
