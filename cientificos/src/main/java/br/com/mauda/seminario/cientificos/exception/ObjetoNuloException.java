package br.com.mauda.seminario.cientificos.exception;

public class ObjetoNuloException extends SeminariosCientificosException {

    public ObjetoNuloException() {
        super("ER0003");

    }

    public ObjetoNuloException(Throwable t) {
        super(t);
    }
}

// throw new SeminariosCientificosException("ER0003"); ->> Não será mais utilizado
// throw new ObjetoNuloException(); ->> Será utilizado